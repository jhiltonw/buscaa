/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscaa;

import java.util.Scanner;

/**
 *
 * @author hilton
 */
public class BuscaA {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner input = new Scanner(System.in);
        Heuristica Executa = new Heuristica();
        
        System.out.print("Digite a cidade onde você está: ");
        String onde = input.nextLine();
        
       
        while(!"E1".equals(onde) && !"E2".equals(onde) && !"E3".equals(onde) && !"E4".equals(onde) && !"E5".equals(onde) && !"E6".equals(onde) && !"E7".equals(onde) && !"E8".equals(onde)
                 && !"E9".equals(onde) && !"E10".equals(onde) && !"E11".equals(onde) && !"E12".equals(onde) && !"E13".equals(onde) && !"E14".equals(onde)){
           
            System.out.println("Opção inválida.");
            System.out.print("Digite a cidade onde você está: ");
            onde = input.nextLine();
        }
        
        System.out.print("Digite o seu Destino: "); 
        String ate = input.nextLine();
        
        while(!"E1".equals(ate) && !"E2".equals(ate) && !"E3".equals(ate) && !"E4".equals(ate) && !"E5".equals(ate) && !"E6".equals(ate) && !"E7".equals(ate) && !"E8".equals(ate)
                 && !"E9".equals(ate) && !"E10".equals(ate) && !"E11".equals(ate) && !"E12".equals(ate) && !"E13".equals(ate) && !"E14".equals(ate)){
            
            System.out.println("Opção inválida.");
            System.out.print("Digite novamente o seu Destino: "); 
            ate = input.nextLine();  
        }
        
        
        Executa.Buscar(onde, ate);
        
            }
    
    
}
